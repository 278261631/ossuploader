package com.ie.taskexecuter;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.scheduling.config.TaskExecutorFactoryBean;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = TaskexecuterApplicationTests.class)

public class TaskexecuterApplicationTests {

	@Test
	public void contextLoads() {
		System.out.println("-----------------------------------");
	}

}
