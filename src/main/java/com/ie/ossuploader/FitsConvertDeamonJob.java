package com.ie.ossuploader;

import com.aliyun.oss.ClientException;
import com.aliyun.oss.OSSClient;
import com.aliyun.oss.model.PutObjectRequest;
import com.aliyun.oss.model.PutObjectResult;
import com.ie.sysmessage.DeviceStatus;
import com.ie.sysmessage.MessageColor;
import com.sun.nio.file.ExtendedWatchEventModifier;
import net.sf.json.JSONObject;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.quartz.*;

import java.io.*;
import java.nio.file.*;
import java.nio.file.attribute.BasicFileAttributes;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;


import static java.nio.file.StandardWatchEventKinds.*;


@PersistJobDataAfterExecution
public class FitsConvertDeamonJob implements Job{
	
	private Log log = LogFactory.getLog(FitsConvertDeamonJob.class);
		
	SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	
	@Override
	public void execute(JobExecutionContext context) throws JobExecutionException {
	    JobDataMap data = context.getJobDetail().getJobDataMap();

		String msg_name_equipment_status=	       data.get("msg_name_equipment_status").toString();
		String device_name=	       data.get("device_name").toString();
		String device_color=	       data.get("device_color").toString();
		String fitsSrcPath=	       data.get("fitsSrcPath").toString();
		String ossPath=	       data.get("ossPath").toString();
		String messageType=	       data.get("messageType").toString();
		Boolean isFileCreate= 		Boolean.valueOf(data.get("isFileCreate").toString());
		Boolean isFileModify= 		Boolean.valueOf(data.get("isFileModify").toString());
		String ossPathFolder=	       data.get("ossPathFolder").toString();
		String bucketName=	       data.get("bucketName").toString();
		List<String> file_extends= (List<String>) data.get("file_extends");

		log.info("本设备 "+device_name+" 监视文件 "+ fitsSrcPath);
//		DeviceStatus deviceStatus = new DeviceStatus("WebCamera" , device_name ,"更新摄像头影像" , sdf.format(new Date()),device_color , MessageColor.OK);

		File fileRoot = new File(fitsSrcPath);
		if (!fileRoot.exists()){
			log.error("dir not exists   目录不存在  "+fitsSrcPath );
			fileRoot.mkdirs();
		}
		if (fileRoot.isFile()){
			log.error(" statusImageRoot 应该是路径而不是文件  "+fitsSrcPath );
		}

		try (WatchService ws = FileSystems.getDefault().newWatchService()) {
			Path dirToWatch = Paths.get(fitsSrcPath);
//			dirToWatch.register(ws, ENTRY_CREATE, ENTRY_MODIFY, ENTRY_DELETE);
			dirToWatch.register(ws, new WatchEvent.Kind[] {StandardWatchEventKinds.ENTRY_CREATE, StandardWatchEventKinds.ENTRY_DELETE, StandardWatchEventKinds.ENTRY_MODIFY}, ExtendedWatchEventModifier.FILE_TREE);
//			Files.walkFileTree(dirToWatch, new SimpleFileVisitor<Path>() {
//				@Override
//				public FileVisitResult preVisitDirectory(Path dir, BasicFileAttributes attrs)
//						throws IOException
//				{
//					dir.register(ws, StandardWatchEventKinds.ENTRY_CREATE, StandardWatchEventKinds.ENTRY_DELETE);
//					return FileVisitResult.CONTINUE;
//				}
//			});

			while (true) {
				WatchKey key = ws.take();
				for (WatchEvent<?> event : key.pollEvents()) {
					WatchEvent.Kind<?> eventKind = event.kind();
					if (eventKind == OVERFLOW) {
						System.out.println("Event  overflow occurred");
						continue;
					}
					WatchEvent<Path> currEvent = (WatchEvent<Path>) event;
//					if(eventKind == StandardWatchEventKinds.ENTRY_CREATE){
//						Path watchable = ((Path) key.watchable()).resolve(currEvent.context());
//						if(Files.isDirectory(watchable)){
//							//注意watchEvent.context()这个只有一个文件名(坑爹啊 为啥不给全路径)
//							watchable.register(ws, StandardWatchEventKinds.ENTRY_CREATE, StandardWatchEventKinds.ENTRY_DELETE);
//						}
//					}

					String fileName = currEvent.context().toString().toUpperCase();
					boolean isFileExtendsPass =false;
					for (String extendsItem: file_extends) {
						if(fileName.endsWith(extendsItem.toUpperCase())){
							isFileExtendsPass=true;
							break;
						}
					}
					if(!isFileExtendsPass){
						log.info("忽略文件 ignore  " +fileName );
						continue;
					}
					if (eventKind == ENTRY_CREATE && isFileCreate) {
						System.out.println("------------------     File Create   -----------------------");
						try {
//							interpreter.p

							Path dirEntry = currEvent.context();
							System.out.println(eventKind + "  occurred on  " + dirEntry);
							System.out.println(eventKind + "  occurred on  " + Paths.get(fitsSrcPath,dirEntry.toString()).toString());
							DeviceStatus deviceStatusOssUpoad = new DeviceStatus(messageType ,device_name , device_name , " oss update  " , sdf.format(new Date()),device_color , MessageColor.OK);
							deviceStatusOssUpoad.setOssPath(ossPath + ossPathFolder + dirEntry.toString().replace('\\','/') );
							if (messageType.equals("oss_thumbnail") || messageType.equals("oss_fits") || messageType.equals("oss_thumbnail_center")){
								deviceStatusOssUpoad.setTaskName(dirEntry.toString().toLowerCase().split("-")[0]);
							}

							String fileFullPath=Paths.get(fitsSrcPath,dirEntry.toString()).toString();
							String ossMiddlePath=new File(fileFullPath).getParent().replace('\\','/').replace(fitsSrcPath.replace('\\','/'),"");
							String ossSubPath=Paths.get(ossPathFolder,ossMiddlePath).toString().replace('\\','/');
							putOss(new File(fileFullPath),ossSubPath ,bucketName);
//							putOss(new File(Paths.get(fitsSrcPath,dirEntry.toString()).toString()),ossPathFolder ,bucketName);
							AmqMessageSender.sendMessage(data, msg_name_equipment_status, JSONObject.fromObject(deviceStatusOssUpoad).toString());
							log.info(deviceStatusOssUpoad.getOssPath());
						} catch (Exception e) {
							log.error(e);
							e.printStackTrace();
							DeviceStatus deviceStatusOssUpoad = new DeviceStatus(messageType , device_name ,device_name , " oss update error  " , sdf.format(new Date()),device_color , MessageColor.Error);
							AmqMessageSender.sendMessage(data, msg_name_equipment_status, JSONObject.fromObject(deviceStatusOssUpoad).toString());
						}
						continue;
					}
					if (eventKind == ENTRY_MODIFY && isFileModify) {
						System.out.println("------------------     File Modify   -----------------------");
						InputStream filepy;
						try {
//							interpreter.p
							Path dirEntry = currEvent.context();
							System.out.println(eventKind + "  occurred on  " + dirEntry);
							System.out.println(eventKind + "  occurred on  " + Paths.get(fitsSrcPath,dirEntry.toString()).toString());
							DeviceStatus deviceStatusOssUpoad = new DeviceStatus(messageType ,device_name , device_name , " oss update  " , sdf.format(new Date()),device_color , MessageColor.OK);
							deviceStatusOssUpoad.setOssPath(ossPath + ossPathFolder + dirEntry.toString().replace('\\','/') );
//							deviceStatusOssUpoad.setMsgType(messageType);
							if (messageType.equals("oss_thumbnail") || messageType.equals("oss_fits") || messageType.equals("oss_thumbnail_center")){
								deviceStatusOssUpoad.setTaskName(dirEntry.toString().toLowerCase().split("-")[0]);

							}

//							if(dirEntry.toString().toLowerCase().contains("_thumbnail.png")){
//								deviceStatusOssUpoad.setMsgType("oss_thumbnail");
//								deviceStatusOssUpoad.setTaskName(dirEntry.toString().toLowerCase().split("-")[0]);
//								ossPathFolder="pngs";
//							}
//							if(dirEntry.toString().toLowerCase().contains("_thumbnail_center_crop.png")){
//								deviceStatusOssUpoad.setMsgType("oss_thumbnail_center");
//								deviceStatusOssUpoad.setTaskName(dirEntry.toString().toLowerCase().split("-")[0]);
//								ossPathFolder="pngs";
//							}
//							if(dirEntry.toString().toLowerCase().endsWith(".fits") || dirEntry.toString().toLowerCase().endsWith(".fts")){
//								deviceStatusOssUpoad.setTaskName("oss_fits");
//								deviceStatusOssUpoad.setTaskName(dirEntry.toString().toLowerCase().split("-")[0]);
//								ossPathFolder="fits";
//							}
							String fileFullPath=Paths.get(fitsSrcPath,dirEntry.toString()).toString();
							String ossMiddlePath=new File(fileFullPath).getParent().replace('\\','/').replace(fitsSrcPath.replace('\\','/'),"");
							String ossSubPath=Paths.get(ossPathFolder,ossMiddlePath).toString().replace('\\','/');
							putOss(new File(fileFullPath),ossSubPath ,bucketName);
							AmqMessageSender.sendMessage(data, msg_name_equipment_status, JSONObject.fromObject(deviceStatusOssUpoad).toString());
							log.info(deviceStatusOssUpoad.getOssPath());
						} catch (Exception e) {

							e.printStackTrace();
							DeviceStatus deviceStatusOssUpoad = new DeviceStatus(messageType ,device_name , device_name , " oss update error  " , sdf.format(new Date()),device_color , MessageColor.Error);
							AmqMessageSender.sendMessage(data, msg_name_equipment_status, JSONObject.fromObject(deviceStatusOssUpoad).toString());
						}
						continue;
					}
//					WatchEvent<Path> currEvent = (WatchEvent<Path>) event;
					Path dirEntry = currEvent.context();
					System.out.println(eventKind + "  occurred on  " + dirEntry);
				}
				boolean isKeyValid = key.reset();
				if (!isKeyValid) {
					System.out.println("No  longer  watching " + dirToWatch);
					break;
				}
			}
		} catch (IOException | InterruptedException e) {
			e.printStackTrace();
		}

	}


	public  String putOss(File file , String ossFolder ,String bucketName){

		String return_key = null;
		try {
			OSSClient client = DefaultOSSClient.getDefaultOSSClient();

			if(file != null){

				String fileName = file.getName();
				/**
				 * getCurrentTimeStamp()方法为同步方法，确保时间戳的唯一性。

				 */
//				String timeStamp = Date2Str.getCurrentTimeStamp();
//				String timeDate = Date2Str.getCurrentDate5();
//				String key = fitsSrcPath + timeDate + timeStamp +"/"+fileName;

//				client.putObject(new PutObjectRequest("open-luna", key, file));
				if(!ossFolder.endsWith("/")){ossFolder = ossFolder+"/";}
				PutObjectResult response = client.putObject(new PutObjectRequest("open-luna", ossFolder+fileName, file));
				response.getETag();
			}

			DefaultOSSClient.shutdownOSSClient();

		} catch (ClientException e) {
			// TODO Auto-generated catch block
			log.info("OSSUpload.put1 error:" + e.toString());
			e.printStackTrace();
			return null;
		}

		return return_key;
	}

}


