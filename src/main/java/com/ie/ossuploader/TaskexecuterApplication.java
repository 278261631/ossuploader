package com.ie.ossuploader;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.apache.activemq.ActiveMQConnection;
import org.apache.activemq.ActiveMQConnectionFactory;
import org.apache.commons.configuration2.AbstractConfiguration;
import org.apache.commons.configuration2.Configuration;
import org.apache.commons.configuration2.builder.fluent.Configurations;
import org.apache.commons.configuration2.convert.DefaultListDelimiterHandler;
import org.apache.commons.configuration2.ex.ConfigurationException;
import javax.jms.*;
import java.io.File;
import org.springframework.stereotype.*;
import org.springframework.beans.factory.annotation.*;

@SpringBootApplication
public class TaskexecuterApplication {
	
	public static void main(String[] args) throws JMSException  {
		SpringApplication.run(TaskexecuterApplication.class, args);
	}
}
