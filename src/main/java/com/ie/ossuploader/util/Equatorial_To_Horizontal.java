package com.ie.taskexecuter.util;

import java.util.Calendar;
import java.util.GregorianCalendar;

public class  Equatorial_To_Horizontal {

	public static double[] Convert_E2H(hmsm hmsm_ra,dms dms_dec,double lat,double longt,Calendar calendar) {
                
	  	astroTime astime=new astroTime();
	  	double local_lst = astime.calc_lst_local(longt, calendar);
//		System.out.println("----local_lst---" + local_lst);    
		double ha = (Math.toRadians(hours_to_degrees(local_lst))-hmsm_ra.rad);
	    ha %= 2 * Math.PI;
//	    System.out.println("----ha  =  "+ha);
	    
	    double slat = Math.sin(Math.toRadians(lat));
	    double clat = Math.cos(Math.toRadians(lat));
	    double sha = Math.sin(ha);
	    double cha = Math.cos(ha);
	    double sdec = Math.sin(dms_dec.rad);
	    double cdec = Math.cos(dms_dec.rad);
//    	
	    
	    double altitude = Math.asin((sdec * slat) + (cdec * clat * cha));
//	     System.out.println( "ALT  in degrees   " + Math.toDegrees(altitude));
	    double Azimuth = Math.acos((sdec - (slat * Math.sin(altitude))) / (clat * Math.cos(altitude)));
	    if (sha > 0){
	    	Azimuth = 2 * Math.PI - Azimuth;	    	
	    }
//	     System.out.println("AZ in degrees    " + Math.toDegrees(Azimuth));
//	     altitude = ((Math.PI / 2. - altitude)+Math.PI)%(2*Math.PI)-Math.PI;
//	    Azimuth = ((Math.PI / 2. - Azimuth)+Math.PI)%(2*Math.PI)-Math.PI;
	    
//	    System.out.println(altitude);
//	    System.out.println(Azimuth);
	    
	    return new double[]{altitude,Azimuth};
	}
	
	static double degrees_to_hours(double degree){
		return degree/15;
	}
	
	static double hours_to_degrees(double degree){
		return degree*15;
	}
}
