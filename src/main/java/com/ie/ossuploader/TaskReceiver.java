package com.ie.ossuploader;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.quartz.JobBuilder;
import org.quartz.JobDetail;
import org.quartz.Scheduler;
import org.quartz.SimpleScheduleBuilder;
import org.quartz.Trigger;
import org.quartz.TriggerBuilder;
import org.quartz.impl.StdSchedulerFactory;

import javax.jms.*;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.springframework.stereotype.*;



import org.springframework.beans.factory.annotation.*;

@ConfigurationProperties
@Component
public class TaskReceiver implements CommandLineRunner{

    private ConnectionFactory connectionFactory;
    private Connection connection = null;
    private Session session;
    private Destination destination;
    private MessageConsumer consumer;
	@Value("${msg_url:failover://(tcp://msg.xtelescope.net:61616)?randomize=false}")
	private String  msg_url;
	@Value("${msg_name: TaskTopic}")
	private String  msg_name;
	@Value("${amqLoginUser: amqLoginUser}")
	private String  amqLoginUser;
	@Value("${amqLoginPass: amqLoginPass}")
	private String  amqLoginPass;
	@Value("${encrypt_key:}")
	private String  encrypt_key;
	

	
	@Value("${msg_name_equipment_status: equipment_status}")
	private String msg_name_equipment_status ;
	@Value("${device_name}")
	private String device_name ;
	@Value("${device_color:#FFAA00}")
	private String device_color ;
//	@Value("${listen_device_list}")
//	private String listen_device_list ;
//	@Value("${isStartWebCamera}")
//	private boolean isStartWebCamera ;
//	@Value("${isStartStatusImage}")
//	private boolean isStartStatusImage ;
	@Value("${isDeleteStatusImage:false}")
	private boolean isDeleteStatusImage ;
//	@Value("${isConvertFits:false}")
//	private boolean isConvertFits ;
//	@Value("${statusImageRoot}")
//	private String statusImageRoot ;
	@Value("${bucketName:open-luna}")
	private String bucketName ;
	@Value("${ossPathFolder}")
	private String ossPathFolder ;
	@Value("${ossPath:http://open-luna.oss-cn-zhangjiakou.aliyuncs.com/}")
	private String ossPath ;
	@Value("#{'${file_extends:png,jpg}'.split(',')}")
	private List<String> file_extends ;
	@Value("${isFileCreate}")
	private Boolean isFileCreate ;
	@Value("${isFileModify}")
	private Boolean isFileModify ;
	@Value("${messageType:oss_fits}")
	private String messageType ;
	@Value("${fitsSrcPath}")
	private String fitsSrcPath ;
//	@Value("${acpFitsRoot}")
//	private String acpFitsRoot ;
//	@Value("${isStartAcpControl}")
//	private boolean isStartAcpControl ;
	@Value("${cameraUpdateSecond:30}")
	private int cameraUpdateSecond ;

    private SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	public void start() throws JMSException  {

	}
	
    @Autowired
    public TaskReceiver(@Value("${msg_url: failover://(tcp://msg.xtelescope.net:61616)?randomize=false}") String msg_url,@Value("${msg_name:loginTopic}") String msg_name,
    		@Value("${amqLoginUser:amqLoginUser}") String amqLoginUser,@Value("${amqLoginPass:amqLoginPass}") String amqLoginPass,
    		@Value("${encrypt_key:}") String encrypt_key,
//    		@Value("${Equipment.lastPlanDir}") String equipment_lastPlanDir,
//    		@Value("${Equipment.latitude}") String equipment_latitude,
//    		@Value("${Equipment.longitude}") String equipment_longitude,
//    		@Value("${Equipment.User}") String equipment_User,
//    		@Value("${Equipment.Pass}") String equipment_Pass,
//    		@Value("${Equipment.LimitAngle_all}") String equipment_LimitAngle_all,
//    		@Value("${Equipment.Url}") String equipment_Url,
//    		@Value("${Equipment.PlanPath}") String equipment_PlanPath,
//    		@Value("${Equipment.filter}") String equipment_filter,
    		@Value("${msg_name_equipment_status:equipment_status}") String msg_name_equipment_status,
    		@Value("${device_name}") String device_name,
    		@Value("${device_color:#FFAA00}") String device_color,
//    		@Value("${listen_device_list}") String listen_device_list,
//    		@Value("${isStartWebCamera}") boolean isStartWebCamera,
    		@Value("${isDeleteStatusImage:false}") boolean isDeleteStatusImage,
//    		@Value("${isConvertFits:false}") boolean isConvertFits,
//    		@Value("${isStartStatusImage}") boolean isStartStatusImage,
//    		@Value("${statusImageRoot}") String statusImageRoot,
    		@Value("${bucketName:open-luna}") String bucketName,
    		@Value("${ossPathFolder}") String ossPathFolder,
    		@Value("${ossPath:http://open-luna.oss-cn-zhangjiakou.aliyuncs.com/}") String ossPath,
    		@Value("#{'${file_extends:png,jpg}'.split(',')}") List<String> file_extends,
    		@Value("${isFileCreate}") Boolean isFileCreate,
    		@Value("${isFileModify}") Boolean isFileModify,
    		@Value("${messageType:oss_fits}") String messageType,
    		@Value("${fitsSrcPath}") String fitsSrcPath,
//    		@Value("${acpFitsRoot}") String acpFitsRoot,
//    		@Value("${isStartAcpControl}") boolean isStartAcpControl,
    		@Value("${cameraUpdateSecond:30}") int cameraUpdateSecond
//    		@Value("${Equipment_safe_east_alt}") String equipment_safe_east_alt,
//    		@Value("${Equipment_safe_east_az}") String equipment_safe_east_az,
//    		@Value("${Equipment_safe_west_alt}") String equipment_safe_west_alt,
//    		@Value("${Equipment_safe_west_az}") String equipment_safe_west_az
    		) {
        this.msg_url = msg_url;
        this.msg_name = msg_name;
        this.amqLoginUser = amqLoginUser;
        this.amqLoginPass = amqLoginPass;
        this.encrypt_key = encrypt_key;


        
        this.msg_name_equipment_status=msg_name_equipment_status;
        this.device_name=device_name;
        this.device_color=device_color;
//        this.listen_device_list=listen_device_list;
//        this.isStartWebCamera=isStartWebCamera;
//        this.isStartStatusImage=isStartStatusImage;
        this.isDeleteStatusImage=isDeleteStatusImage;
//        this.isConvertFits=isConvertFits;
//        this.statusImageRoot=statusImageRoot;
        this.bucketName=bucketName;
        this.ossPathFolder=ossPathFolder;
        this.ossPath=ossPath;
        this.file_extends=file_extends;
        this.messageType=messageType;
        this.isFileCreate=isFileCreate;
        this.isFileModify=isFileModify;
        this.fitsSrcPath=fitsSrcPath;
//        this.acpFitsRoot=acpFitsRoot;
//        this.isStartAcpControl=isStartAcpControl;
        this.cameraUpdateSecond=cameraUpdateSecond;

        log.info("+++++++++++++++++++++++++++++++++++++++++++++++++++");
        log.info("msg_url                " + msg_url );
        log.info("msg_name               " + msg_name );
        log.info("amqLoginUser           " + amqLoginUser );
        log.info("amqLoginPass           " + amqLoginPass );
        log.info("encrypt_key            " + encrypt_key );
//        log.info("equipment_Url          " + equipment_Url );
//        log.info("equipment_User         " + equipment_User );
//        log.info("equipment_Pass         " + equipment_Pass );
        log.info("file_extends         " + file_extends );
    }
   

    private Log log = LogFactory.getLog(TaskReceiver.class);
    SimpleDateFormat simpleDateFormat=new SimpleDateFormat("yyyy-MM-dd HH mm ss SSS");
	@Override
	public void run(String... arg0) throws Exception {

		log.info("Server Start           "+simpleDateFormat.format(new Date()));
		log.info("+++++++++++++++++++++++++++++++++++++++++++++++++++");
		log.info("暂时不支持多级目录 等待更新");
		log.info("Start EquipmentDeamon Schedule...");
		Scheduler sched = new StdSchedulerFactory().getScheduler();
        sched.start();


            log.info("  fits Converter   启动fits文件转换");
//            JobDetail fitsConvertJob = JobBuilder.newJob(FitsConvertDeamonJob.class).storeDurably()
//                    .withIdentity("FitsConverter_Job", "EquipmentDeamonJob_JobGroup").build();
            JobDetail fitsConvertJob = JobBuilder.newJob(GWFileDeamonJob.class).storeDurably()
                    .withIdentity("FitsConverter_Job", "EquipmentDeamonJob_JobGroup").build();

            fitsConvertJob.getJobDataMap().put("SysConfig.msg_url",this.msg_url);
            fitsConvertJob.getJobDataMap().put("SysConfig.msg_name",this.msg_name);
            fitsConvertJob.getJobDataMap().put("msg_name_equipment_status",msg_name_equipment_status);
            fitsConvertJob.getJobDataMap().put("device_name",device_name);
            fitsConvertJob.getJobDataMap().put("device_color",device_color);

            fitsConvertJob.getJobDataMap().put("fitsSrcPath",fitsSrcPath);
            fitsConvertJob.getJobDataMap().put("ossPath",ossPath);
            fitsConvertJob.getJobDataMap().put("file_extends",file_extends);
            fitsConvertJob.getJobDataMap().put("messageType",messageType);
            fitsConvertJob.getJobDataMap().put("isFileCreate",isFileCreate);
            fitsConvertJob.getJobDataMap().put("isFileModify",isFileModify);
            fitsConvertJob.getJobDataMap().put("ossPathFolder",ossPathFolder);
            fitsConvertJob.getJobDataMap().put("bucketName",bucketName);

            fitsConvertJob.getJobDataMap().put("msg_url",msg_url);
            fitsConvertJob.getJobDataMap().put("msg_name",msg_name);


//            DeviceStatus deviceStatus = new DeviceStatus("oss" , device_name , " oss update  " , sdf.format(new Date()),device_color , MessageColor.OK);
//            AmqMessageSender.sendMessage(fitsConvertJob.getJobDataMap(), msg_name_equipment_status,JSONObject.fromObject(deviceStatus).toString());

            Trigger triggerFitsConverter = TriggerBuilder.newTrigger().withIdentity("FitsConverter_Deamon_Trigger", "EquipmentDeamonTrigger_TriggerGroup")
                    .startNow().withSchedule(SimpleScheduleBuilder.simpleSchedule().withIntervalInSeconds(cameraUpdateSecond)
                            .withRepeatCount(0)).build();
            sched.scheduleJob(fitsConvertJob, triggerFitsConverter);

	}
}
