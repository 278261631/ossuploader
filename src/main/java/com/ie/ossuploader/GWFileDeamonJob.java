package com.ie.ossuploader;

import com.aliyun.oss.ClientException;
import com.aliyun.oss.OSSClient;
import com.aliyun.oss.model.PutObjectRequest;
import com.aliyun.oss.model.PutObjectResult;
import com.ie.sysmessage.DeviceStatus;
import com.ie.sysmessage.MessageColor;
import com.ie.sysmessage.TaskData;
import com.sun.nio.file.ExtendedWatchEventModifier;
import com.sun.nio.file.SensitivityWatchEventModifier;
import net.sf.json.JSONObject;
import org.apache.activemq.ActiveMQConnection;
import org.apache.activemq.ActiveMQConnectionFactory;
import org.apache.commons.io.FileUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.quartz.*;

import javax.jms.*;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.*;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import static java.nio.file.StandardWatchEventKinds.*;


@PersistJobDataAfterExecution
public class GWFileDeamonJob implements Job{
	
	private Log log = LogFactory.getLog(GWFileDeamonJob.class);
		
	SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	
	@Override
	public void execute(JobExecutionContext context) throws JobExecutionException {
	    JobDataMap data = context.getJobDetail().getJobDataMap();

		String msg_name_equipment_status=	       data.get("msg_name_equipment_status").toString();
		String device_name=	       data.get("device_name").toString();
		String device_color=	       data.get("device_color").toString();
		String fitsSrcPath=	       data.get("fitsSrcPath").toString();
		String ossPath=	       data.get("ossPath").toString();
		String messageType=	       data.get("messageType").toString();
		Boolean isFileCreate= 		Boolean.valueOf(data.get("isFileCreate").toString());
		Boolean isFileModify= 		Boolean.valueOf(data.get("isFileModify").toString());
		String ossPathFolder=	       data.get("ossPathFolder").toString();
		String bucketName=	       data.get("bucketName").toString();

		String msg_url=	       data.get("msg_url").toString();
		String msg_name=	       data.get("msg_name").toString();


		List<String> file_extends= (List<String>) data.get("file_extends");

		log.info("本设备 "+device_name+" 监视文件 "+ fitsSrcPath);

		File fileRoot = new File(fitsSrcPath);
		if (!fileRoot.exists()){
			log.error("dir not exists   目录不存在  "+fitsSrcPath );
			fileRoot.mkdirs();
		}
		if (fileRoot.isFile()){
			log.error(" statusImageRoot 应该是路径而不是文件  "+fitsSrcPath );
		}

		try (WatchService ws = FileSystems.getDefault().newWatchService()) {
			Path dirToWatch = Paths.get(fitsSrcPath);
//			dirToWatch.register(ws, new WatchEvent.Kind[] {StandardWatchEventKinds.ENTRY_CREATE, StandardWatchEventKinds.ENTRY_DELETE, StandardWatchEventKinds.ENTRY_MODIFY}, ExtendedWatchEventModifier.FILE_TREE);
			dirToWatch.register(ws, new WatchEvent.Kind[] {StandardWatchEventKinds.ENTRY_CREATE, StandardWatchEventKinds.ENTRY_DELETE, StandardWatchEventKinds.ENTRY_MODIFY}, SensitivityWatchEventModifier.MEDIUM);
			while (true) {
				WatchKey key = ws.take();
				for (WatchEvent<?> event : key.pollEvents()) {
					WatchEvent.Kind<?> eventKind = event.kind();
					if (eventKind == OVERFLOW) {
						System.out.println("Event  overflow occurred");
						continue;
					}
					WatchEvent<Path> currEvent = (WatchEvent<Path>) event;
					String fileName = currEvent.context().toString().toUpperCase();
					boolean isFileExtendsPass =false;
					for (String extendsItem: file_extends) {
						if(fileName.endsWith(extendsItem.toUpperCase())){
							isFileExtendsPass=true;
							break;
						}
					}
					if(!isFileExtendsPass){
						log.info("忽略文件 ignore  " +fileName );
						continue;
					}
					if (eventKind == ENTRY_CREATE && isFileCreate) {
						System.out.println("------------------     File Create   -----------------------");
						try {
							Path dirEntry = currEvent.context();
							System.out.println(eventKind + "  occurred on  " + dirEntry);
							System.out.println(eventKind + "  occurred on  " + Paths.get(fitsSrcPath,dirEntry.toString()).toString());


							String fileFullPath=Paths.get(fitsSrcPath,dirEntry.toString()).toString();
							File createdFile = new File(fileFullPath);
							String ossMiddlePath=new File(fileFullPath).getParent().replace('\\','/').replace(fitsSrcPath.replace('\\','/'),"");
							String ossSubPath=Paths.get(ossPathFolder,ossMiddlePath).toString().replace('\\','/');
//							putOss(new File(fileFullPath),ossSubPath ,bucketName);
							log.info("file =   "+ createdFile.getAbsolutePath());
							try {
								waitForWirtenCompleted(new File(createdFile.getAbsolutePath()));
								String amqMsg = FileUtils.readFileToString(createdFile,"UTF-8");
								log.info(amqMsg);
								TaskData gwTask = new TaskData();
								gwTask.setTask_level("999");
								gwTask.setTaskName("GW_"+sdf.format(new Date())+fileName);
								gwTask.setTask_Ra_deg(0);
								gwTask.setTask_Dec_deg(0);
								gwTask.setTarget_eqp("GW_EQP");
								gwTask.setTask_type("GRAVITATIONALWAVE");
								gwTask.setTask_plan_text(amqMsg);
								JSONObject jsonObject=JSONObject.fromObject(gwTask);
								sendAMQ(msg_url, msg_name, jsonObject.toString());
							} catch (IOException e) {
									e.printStackTrace();
							}


							} catch (Exception e) {
							log.error(e);
							e.printStackTrace();
						}
						continue;
					}
					if (eventKind == ENTRY_MODIFY && isFileModify) {
						System.out.println("------------------     File Modify   -----------------------");
						continue;
					}
//					WatchEvent<Path> currEvent = (WatchEvent<Path>) event;
					Path dirEntry = currEvent.context();
					System.out.println(eventKind + "  occurred on  " + dirEntry);
				}
				boolean isKeyValid = key.reset();
				if (!isKeyValid) {
					System.out.println("No  longer  watching " + dirToWatch);
					break;
				}
			}
		} catch (IOException | InterruptedException e) {
			e.printStackTrace();
		}

	}

	private void waitForWirtenCompleted(File file) {
		if (!file.exists()){
			return;
		}
		long old_length;
		do {
			old_length = file.length();
			try {
				Thread.sleep(300);
				log.info("wait file ............... " + old_length  + "      " + file.length());
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		} while (old_length != file.length());
		log.info("wait file ...................................OK    "+old_length);
	}

	public void sendAMQ(String amqURL, String gravitationalWaveTopic, String jsonMessage){
		ConnectionFactory connectionFactory;
		Connection connection = null;
		Session session;
		Destination destination;
		MessageProducer producer;
		TextMessage message = null;
		connectionFactory = new ActiveMQConnectionFactory(ActiveMQConnection.DEFAULT_USER
				, ActiveMQConnection.DEFAULT_PASSWORD, amqURL);
		try {
			log.info("...........AMQ Send  Start............."+gravitationalWaveTopic + "         " + amqURL);
			connection = connectionFactory.createConnection();
			connection.start();
			session = connection.createSession(Boolean.TRUE, Session.AUTO_ACKNOWLEDGE);
			destination = session.createTopic(gravitationalWaveTopic);
			producer = session.createProducer(destination);
			//todo use json text message
			message=session.createTextMessage(jsonMessage);
			producer.send(message);
			session.commit();
			log.info("...........AMQ Send  End.............");
		} catch (JMSException e) {
			log.error(e);
			e.printStackTrace();
		}finally {
			try {
				if (null != connection){
					connection.close();
				}
			} catch (Throwable ignore) {
			}
		}
	}

}


